/**
 * This macro is for Dinosaur Racing in Tomb of Annihilation.
 * It uses custom rules outlined here https://blog.balthazar-rouberol.com/running-the-port-nyanzaru-dinosaur-race
 * 
 * When you run this macro, it will prompt you with a form to fill out with dino stats which you can find in the above link.
 * Clicking the "Set Stats" button on the prompt will store a `dinoRiderStats` object on the currently selected actor. 
 * This is only stored in the browser so refreshing the page will require you to run this macro again.
 * 
 * Once this macro has been run for all the actors participating in the race, you can then use the `dino-race` macro during the race.
 */

if (!actor) {
    ui.notifications.error("You must have a token selected.");
} else {

    let dinoStatBlocks = {
        "Triceratops": {
            dinosaur: `Triceratops`,
            slowDice: `1d6`,
            fastDice: `1d4+6`,
            animalHandlingDC: 14,
            conMod: `+2`,
            dexMod: `-1`,
            critBonusMod: `+1d4`,
            critFailMod: `*0`
        },
        "Hadrosaurus": {
            dinosaur: `Hadrosaurus`,
            slowDice: `1d6`,
            fastDice: `1d2+6`,
            animalHandlingDC: 10,
            conMod: `+1`,
            dexMod: `+0`,
            critBonusMod: `+1d2`,
            critFailMod: `*0`
        },
        "Tyrannosaurus": {
            dinosaur: `Tyrannosaurus`,
            slowDice: `1d6`,
            fastDice: `1d6+6`,
            animalHandlingDC: 18,
            conMod: `+3`,
            dexMod: `+0`,
            critBonusMod: `+1d6`,
            critFailMod: `*0`
        },
        "Dimetrodon": {
            dinosaur: `Dimetrodon`,
            slowDice: `1d4`,
            fastDice: `1d4+4`,
            animalHandlingDC: 8,
            conMod: `+2`,
            dexMod: `+1`,
            critBonusMod: `+1d4`,
            critFailMod: `*0`
        },
        "Allosaurus": {
            dinosaur: `Allosaurus`,
            slowDice: `1d6`,
            fastDice: `1d4+6`,
            animalHandlingDC: 16,
            conMod: `+2`,
            dexMod: `+1`,
            critBonusMod: `+1d4`,
            critFailMod: `*0`
        },
        "Deinonychus": {
            dinosaur: `Deinonychus`,
            slowDice: `1d6`,
            fastDice: `1d2+6`,
            animalHandlingDC: 12,
            conMod: `+2`,
            dexMod: `+2`,
            critBonusMod: `+1d2`,
            critFailMod: `*0`
        },
        "Ankylosaurus": {
            dinosaur: `Ankylosaurus`,
            slowDice: `1d4`,
            fastDice: `1d6+4`,
            animalHandlingDC: 13,
            conMod: `+2`,
            dexMod: `+0`,
            critBonusMod: `+1d6`,
            critFailMod: `*0`
        },
        "Velociraptor": {
            dinosaur: `Velociraptor`,
            slowDice: `1d8`,
            fastDice: `1d2+8`,
            animalHandlingDC: 12,
            conMod: `+1`,
            dexMod: `+2`,
            critBonusMod: `+1d8`,
            critFailMod: `*0`
        }
    };



    // this is the content shown in the prompt
    var promptContent = `
  <form>
    <div style="display: flex; width: 100%; margin-bottom: 10px">
<select name="dinoChoice" id="dinoChoice">
`

    var dinoNames = Object.keys(dinoStatBlocks);
    dinoNames.sort();
    dinoNames.forEach(dinoName => {
        promptContent += `<option value="${dinoName}">${dinoName}</option>`
    });

    promptContent += `
</select>
  </form>
`

    // this is executed when you click the `Set Stats` button
    let saveStateFunction = (html) => {

        let selectedDinoName = html.find("#dinoChoice").val();
        actor.dinoRiderStats = dinoStatBlocks[selectedDinoName];


        // read them from the actor, and whisper them to to verify they're set correctly
        ChatMessage.create({
            user: game.user._id,
            content: `<b>Set Dino Rider Stats for actor: ${actor.name}</b><br/>
                {<br/>  
                &nbsp;&nbsp;  dinosaur: ${actor.dinoRiderStats.dinosaur},<br/>
                &nbsp;&nbsp;  slowDice: ${actor.dinoRiderStats.slowDice},<br/>
                &nbsp;&nbsp;  fastDice: ${actor.dinoRiderStats.fastDice},<br/>
                &nbsp;&nbsp;  animalHandlingDC: ${actor.dinoRiderStats.animalHandlingDC},<br/>
                &nbsp;&nbsp;  conMod: ${actor.dinoRiderStats.conMod},<br/>
                &nbsp;&nbsp;  dexMod: ${actor.dinoRiderStats.dexMod},<br/>
                &nbsp;&nbsp;  critBonusMod: ${actor.dinoRiderStats.critBonusMod},<br/>
                &nbsp;&nbsp;  critFailMod: ${actor.dinoRiderStats.critFailMod},<br/>
                }`,
            type: CONST.CHAT_MESSAGE_TYPES.WHISPER
        });
    }

    // show the prompt
    new Dialog({
        title: `Dino Racing Stats`,
        content: promptContent,
        buttons: {
            yes: {
                icon: "<i class='fas fa-check'></i>",
                label: `Set Stats`,
                callback: saveStateFunction
            },
            no: {
                icon: "<i class='fas fa-times'></i>",
                label: `Cancel`
            }
        }
    }).render(true)

}
