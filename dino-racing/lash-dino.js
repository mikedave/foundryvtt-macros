/** 
 * Don't forget to run the `Set Dino Stats` Macro for each player in the race
 *
 * This macro is for Dinosaur Racing in Tomb of Annihilation.
 * It uses custom rules outlined here https://blog.balthazar-rouberol.com/running-the-port-nyanzaru-dinosaur-race
 * 
 * When you run this macro, a DC 10 CON save will be rolled using the conMod specified in the `Set Dino Stats` macro.
 * If the save succeeds, it will roll the "Fast Dice" formula specified in the `Set Dino Stats` macro.
 * If the save fails, it will roll the "Fast Dice" formula specified in the `Set Dino Stats` macro, then divide it by 2 rounded down.
 * 
 * A message with both rolls will then be posted to the chat.
 */

if (!actor) {
    ui.notifications.error("You must have a token selected.");
} else if (!actor.dinoRiderStats) {
    ui.notifications.error(`<b>${actor.name}</b> doesn't have dino stats yet! Try running the <b>Set Dino Stats</b> macro`);
} else {

    let dinoStats = actor.dinoRiderStats;
    let conSaveRoll = new Roll(`1d20${dinoStats.conMod}`).roll();
    let conSaveDC = 10;
    let conSaveFlavor = `DC ${conSaveDC} CON Save`;

    var movementFlavor = ``;
    var movementFormula = `${dinoStats.fastDice}`;
    if (conSaveRoll.total >= conSaveDC) {
        // successful CON save
        movementFlavor = `<br/>${actor.name} lashes their ${dinoStats.dinosaur} and it moves quickly`;
    } else {
        // failed CON save
        movementFlavor = `<br/>${actor.name} lashes their ${dinoStats.dinosaur} too hard and it moves at half speed for the rest of the turn`;
        movementFormula = `floor((${dinoStats.fastDice}) / 2)`;
    }

    // roll the movement formula that we just built
    let movementRoll = new Roll(`${movementFormula}`).roll();
    // display both rolls in the chat
    conSaveRoll.render({ flavor: conSaveFlavor }).then((conSaveHtml) => {
        movementRoll.toMessage({
            flavor: conSaveHtml + movementFlavor,
            speaker: ChatMessage.getSpeaker({ token: actor })
        });
    });
}
