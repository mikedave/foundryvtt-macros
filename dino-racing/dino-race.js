/** 
 * Don't forget to run the `Set Dino Stats` Macro for each player in the race
 *
 * This macro is for Dinosaur Racing in Tomb of Annihilation.
 * It uses custom rules outlined here https://blog.balthazar-rouberol.com/running-the-port-nyanzaru-dinosaur-race
 * 
 * When you run this macro, you will be prompted with an optional field that allows you to enter the value of a manual roll in case a player uses real dice.
 * 
 * When you click the "Roll" button, it will roll an animal handling check using the currently selected actor.
 * If the check succeeds, it will roll the "Fast Dice" formula specified in the `Set Dino Stats` macro.
 * If the check is a natural 20, it will append the "Critical Success Modifier" to the "Fast Dice" formula before rolling.
 * If the check fails, it will roll the "Slow Dice" formula specified in the `Set Dino Stats` macro.
 * If the check is a natural 1, it will append the "Critical Failure Modifier" to the "Slow Dice" formula before rolling.
 * 
 * A message with both rolls will then be posted to the chat.
 */

if (!actor) {
  ui.notifications.error("You must have a token selected.");
} else if (!actor.dinoRiderStats) {
  ui.notifications.error(`<b>${actor.name}</b> doesn't have dino stats yet! Try running the <b>Set Dino Stats</b> macro`);
} else {

  // this is the content shown in the prompt
  let promptContent = `
  <form>
    <div style="display: flex; width: 100%; margin-bottom: 10px">
      <label for="manual-roll" style="white-space: nowrap; margin-right: 10px; padding-top:4px">Manual Roll Value:</label>
      <input type="text" id="manual-roll" name="manual-roll" />
    </div>

    <div style="display: flex; width: 100%; margin-bottom: 10px">
      <label for="custom-mod" style="white-space: nowrap; margin-right: 10px; padding-top:4px">Custom Modifier (eg. +1d4):</label>
      <input type="text" id="custom-mod" name="custom-mod" />
    </div>

    <div style="display: flex; width: 100%; margin-bottom: 10px">
      <input type="radio" name="rollType" value="normal" checked>&nbsp;Normal&nbsp;&nbsp;
      <input type="radio" name="rollType" value="advantage">&nbsp;Advantage&nbsp;&nbsp;
      <input type="radio" name="rollType" value="disadvantage">&nbsp;Disadvantage&nbsp;&nbsp;
    </div>
  
    <div style="display: flex; width: 100%; margin-bottom: 10px">
      <input type="checkbox" id="stopPrompting" name="stopPrompting" value="stopPrompting" />
      <label for="stopPrompting" style="white-space: nowrap; margin-right: 10px; padding-top:4px">
        Stop prompting for this actor<br/>(run Set Dino Stats again to re-enable this prompt)
      </label>
    </div>

  </form>
  `

  // this is executed when you click the Roll! button
  let rollFunction = (manualRollValue = -1, rollType = "normal", customMod = ``) => {

    let dinoStats = actor.dinoRiderStats;
    let greenMessage = (message) => { return `<span style="color:green;">${message}</span>` };
    let redMessage = (message) => { return `<span style="color:red;">${message}</span>` };

    let rollFormula = `1d20`;
    if (manualRollValue > 0) {
      // they specified a manual roll so use that instead of rolling a d20
      rollFormula = `{${manualRollValue}}`;
    } else if (rollType == "advantage") {
      rollFormula = `2d20kh`;
    } else if (rollType == "disadvantage") {
      rollFormula = `2d20kl`;
    }
    rollFormula += `${customMod}`

    // Roll an animal handling check using the currently selected actor's animal handling modifier
    let animalHandlingRoll = new Roll(`${rollFormula} + ${actor.data.data.skills.ani.total}`).roll();
    // grab the natural roll so we can check if it's a 20 or a 1 later.
    let natRoll = animalHandlingRoll.terms[0].total;
    // this will be displayed above the animal handling roll
    let animalHandlingFlavor = `DC ${dinoStats.animalHandlingDC} Animal Handling Check`;

    // Determine the roll formula to use for movement, and the flavor text that will be displayed above it.
    var movementFormula = ``;
    var movementFlavor = ``;
    if (natRoll == 20) {
      // critical success!
      movementFlavor = greenMessage(`<br/><b>Critical Success!</b> ${actor.name} moves very quickly`);
      movementFormula = `(${dinoStats.fastDice})${dinoStats.critBonusMod}`;
    } else if (animalHandlingRoll.total >= dinoStats.animalHandlingDC) {
      // successful Animal Handling Check
      movementFormula = `${dinoStats.fastDice}`;
      movementFlavor = `<br/>Success! ${actor.name} moves quickly`;
    } else if (natRoll == 1) {
      // critical failure
      movementFormula = `(${dinoStats.slowDice})${dinoStats.critFailMod}`;
      movementFlavor = redMessage(`<br/><b>Critical Failure!</b> `);
    } else {
      // failed animal handling check
      movementFormula = `${dinoStats.slowDice}`;
      movementFlavor = `<br/>Failure! ${actor.name} moves slowly`;
    }


    // roll the movement formula that we just built
    let movementRoll = new Roll(`${movementFormula}`).roll();

    // display both rolls in the chat as the actor
    animalHandlingRoll.render({ flavor: animalHandlingFlavor }).then((animalHandlingHtml) => {
      movementRoll.toMessage({
        flavor: animalHandlingHtml + movementFlavor,
        speaker: ChatMessage.getSpeaker({ token: actor })
      });
    });
  };

  if (actor.dinoRiderStats.stopPrompting) {
    // the user checked the stop prompting checkbox so just execute the rollFunction
    rollFunction();
  } else {
    // prompt the user for manual roll input
    new Dialog({
      title: `Dino Roll for ${actor.name}`,
      content: promptContent,
      buttons: {
        yes: {
          icon: "<i class='fas fa-check'></i>",
          label: `Roll!`,
          callback: (html) => {
            // whether or not to stop prompting for this actor
            actor.dinoRiderStats.stopPrompting = html.find('#stopPrompting').prop("checked");
            let manualRollValue = parseInt(html.find('#manual-roll').val());
            let rollType = html.find("input[type='radio'][name='rollType']:checked").val();
            let customMod = html.find('#custom-mod').val();
            rollFunction(manualRollValue, rollType, customMod);
          }
        },
        no: {
          icon: "<i class='fas fa-times'></i>",
          label: `Cancel`
        }
      }
    }).render(true)
  }
}
