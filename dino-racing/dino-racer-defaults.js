let dinoStatBlocks = {
    "Triceratops": {
        dinosaur: `Triceratops`,
        slowDice: `1d6`,
        fastDice: `1d4+6`,
        animalHandlingDC: 14,
        conMod: `+2`,
        dexMod: `-1`,
        critBonusMod: `+1d4`,
        critFailMod: `*0`
    },
    "Hadrosaurus": {
        dinosaur: `Hadrosaurus`,
        slowDice: `1d6`,
        fastDice: `1d2+6`,
        animalHandlingDC: 10,
        conMod: `+1`,
        dexMod: `+0`,
        critBonusMod: `+1d2`,
        critFailMod: `*0`
    },
    "Tyrannosaurus": {
        dinosaur: `Tyrannosaurus`,
        slowDice: `1d6`,
        fastDice: `1d6+6`,
        animalHandlingDC: 18,
        conMod: `+3`,
        dexMod: `+0`,
        critBonusMod: `+1d6`,
        critFailMod: `*0`
    },
    "Dimetrodon": {
        dinosaur: `Dimetrodon`,
        slowDice: `1d4`,
        fastDice: `1d4+4`,
        animalHandlingDC: 8,
        conMod: `+2`,
        dexMod: `+1`,
        critBonusMod: `+1d4`,
        critFailMod: `*0`
    },
    "Allosaurus": {
        dinosaur: `Allosaurus`,
        slowDice: `1d6`,
        fastDice: `1d4+6`,
        animalHandlingDC: 16,
        conMod: `+2`,
        dexMod: `+1`,
        critBonusMod: `+1d4`,
        critFailMod: `*0`
    },
    "Deinonychus": {
        dinosaur: `Deinonychus`,
        slowDice: `1d6`,
        fastDice: `1d2+6`,
        animalHandlingDC: 12,
        conMod: `+2`,
        dexMod: `+2`,
        critBonusMod: `+1d2`,
        critFailMod: `*0`
    },
    "Ankylosaurus": {
        dinosaur: `Ankylosaurus`,
        slowDice: `1d4`,
        fastDice: `1d6+4`,
        animalHandlingDC: 13,
        conMod: `+2`,
        dexMod: `+0`,
        critBonusMod: `+1d6`,
        critFailMod: `*0`
    },
    "Velociraptor": {
        dinosaur: `Velociraptor`,
        slowDice: `1d8`,
        fastDice: `1d2+8`,
        animalHandlingDC: 12,
        conMod: `+1`,
        dexMod: `+2`,
        critBonusMod: `+1d8`,
        critFailMod: `*0`
    }
};

let racers = {
    "Atuar": "Triceratops",
    "Azuil": "Hadrosaurus",
    "Chuil": "Tyrannosaurus",
    "Gavori": "Dimetrodon",
    "Fipya": "Allosaurus",
    "Sana": "Deinonychus",
    "Kwalu": "Ankylosaurus",
    "Tiryk": "Velociraptor"
};

Object.keys(racers).forEach((racerName) => {
    let foundActor = game.actors.find((a) => a.name == racerName)
    if (foundActor) {
        let selectedDinoName = racers[racerName];
        foundActor.dinoRiderStats = dinoStatBlocks[selectedDinoName];

        // read them from the actor, and whisper them to to verify they're set correctly
        ChatMessage.create({
            user: game.user._id,
            content: `<b>Set Dino Rider Stats for actor: ${foundActor.name}</b><br/>
                {<br/>  
                &nbsp;&nbsp;  dinosaur: ${foundActor.dinoRiderStats.dinosaur},<br/>
                &nbsp;&nbsp;  slowDice: ${foundActor.dinoRiderStats.slowDice},<br/>
                &nbsp;&nbsp;  fastDice: ${foundActor.dinoRiderStats.fastDice},<br/>
                &nbsp;&nbsp;  animalHandlingDC: ${foundActor.dinoRiderStats.animalHandlingDC},<br/>
                &nbsp;&nbsp;  conMod: ${foundActor.dinoRiderStats.conMod},<br/>
                &nbsp;&nbsp;  dexMod: ${foundActor.dinoRiderStats.dexMod},<br/>
                &nbsp;&nbsp;  critBonusMod: ${foundActor.dinoRiderStats.critBonusMod},<br/>
                &nbsp;&nbsp;  critFailMod: ${foundActor.dinoRiderStats.critFailMod},<br/>
                }`,
            type: CONST.CHAT_MESSAGE_TYPES.WHISPER
        });


    }
});
