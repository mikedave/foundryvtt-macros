/**
 * This macro is for Dinosaur Racing in Tomb of Annihilation.
 * It uses custom rules outlined here https://blog.balthazar-rouberol.com/running-the-port-nyanzaru-dinosaur-race
 * 
 * When you run this macro, it will prompt you with a form to fill out with dino stats which you can find in the above link.
 * Clicking the "Set Stats" button on the prompt will store a `dinoRiderStats` object on the currently selected actor. 
 * This is only stored in the browser so refreshing the page will require you to run this macro again.
 * 
 * Once this macro has been run for all the actors participating in the race, you can then use the `dino-race` macro during the race.
 */

if (!actor) {
  ui.notifications.error("You must have a token selected.");
} else {

  // this is the content shown in the prompt
  let promptContent = `
  <form>

    <div style="display: flex; width: 100%; margin-bottom: 10px">
      <input type="text" id="dinoName" name="dinoName" value="Dinosaur" style="width:20%" />
      <label for="dinoName" style="white-space: nowrap; margin-left: 10px; padding-top:4px">Dinosaur Name: </label>
    </div>

    <div style="display: flex; width: 100%; margin-bottom: 10px">
      <input type="text" id="slowDice" name="slowDice" value="1d6" style="width:20%" />
      <label for="slowDice" style="white-space: nowrap; margin-left: 10px; padding-top:4px">Slow Dice: </label>
    </div>

    <div style="display: flex; width: 100%; margin-bottom: 10px">
      <input type="text" id="fastDice" name="fastDice" value="1d4+6" style="width:20%" />
      <label for="fastDice" style="white-space: nowrap; margin-left: 10px; padding-top:4px">Fast Dice: </label>
    </div>

    <div style="display: flex; width: 100%; margin-bottom: 10px">
      <input type="text" id="animalHandlingDC" name="animalHandlingDC" value="14" style="width:20%" />
      <label for="animalHandlingDC" style="white-space: nowrap; margin-left: 10px; padding-top:4px">Animal Handling DC: </label>
    </div>

    <div style="display: flex; width: 100%; margin-bottom: 10px">
      <input type="text" id="conMod" name="conMod" value="+2" style="width:20%" />
      <label for="conMod" style="white-space: nowrap; margin-left: 10px; padding-top:4px">Dino CON save mod: </label>
    </div>

    <div style="display: flex; width: 100%; margin-bottom: 10px">
      <input type="text" id="dexMod" name="dexMod" value="-1" style="width:20%" />
      <label for="dexMod" style="white-space: nowrap; margin-left: 10px; padding-top:4px">Dino DEX save mod: </label>
    </div>

    <div style="display: flex; width: 100%; margin-bottom: 10px">
      <input type="text" id="critBonusMod" name="critBonusMod" value="+1d4" style="width:20%" />
      <label for="critBonusMod" style="white-space: nowrap; margin-left: 10px; padding-top:4px">Critical Success Modifier<br/>(appended to the end of Fast Dice, optional): </label>
    </div>

    <div style="display: flex; width: 100%; margin-bottom: 10px">
      <input type="text" id="critFailMod" name="critFailMod" value="*0" style="width:20%" />
      <label for="critFailMod" style="white-space: nowrap; margin-left: 10px; padding-top:4px">Critical Failure Modifier<br/>(appended to the end of Slow Dice, optional): </label>
    </div>

  </form>
`

  // this is executed when you click the `Set Stats` button
  let saveStateFunction = (html) => {
    // collect all the values from the submitted form
    let dinoName = html.find('#dinoName').val();
    let slowDice = html.find('#slowDice').val();
    let fastDice = html.find('#fastDice').val();
    let animalHandlingDC = html.find('#animalHandlingDC').val();
    let conMod = html.find('#conMod').val();
    let dexMod = html.find('#dexMod').val();
    let critBonusMod = html.find('#critBonusMod').val();
    let critFailMod = html.find('#critFailMod').val();

    // set them on the selected actor
    actor.dinoRiderStats = {
      dinosaur: `${dinoName}`,
      slowDice: `${slowDice}`,
      fastDice: `${fastDice}`,
      animalHandlingDC: parseInt(animalHandlingDC),
      conMod: `${conMod}`,
      dexMod: `${dexMod}`,
      critBonusMod: `${critBonusMod}`,
      critFailMod: `${critFailMod}`
    };

    // read them from the actor, and whisper them to to verify they're set correctly
    ChatMessage.create({
      user: game.user._id,
      content: `<b>Set Dino Rider Stats for actor: ${actor.name}</b><br/>
                {<br/>  
                &nbsp;&nbsp;  dinosaur: ${actor.dinoRiderStats.dinosaur},<br/>
                &nbsp;&nbsp;  slowDice: ${actor.dinoRiderStats.slowDice},<br/>
                &nbsp;&nbsp;  fastDice: ${actor.dinoRiderStats.fastDice},<br/>
                &nbsp;&nbsp;  animalHandlingDC: ${actor.dinoRiderStats.animalHandlingDC},<br/>
                &nbsp;&nbsp;  conMod: ${actor.dinoRiderStats.conMod},<br/>
                &nbsp;&nbsp;  dexMod: ${actor.dinoRiderStats.dexMod},<br/>
                &nbsp;&nbsp;  critBonusMod: ${actor.dinoRiderStats.critBonusMod},<br/>
                &nbsp;&nbsp;  critFailMod: ${actor.dinoRiderStats.critFailMod},<br/>
                }`,
      type: CONST.CHAT_MESSAGE_TYPES.WHISPER
    });
  }

  // show the prompt
  new Dialog({
    title: `Dino Racing Stats`,
    content: promptContent,
    buttons: {
      yes: {
        icon: "<i class='fas fa-check'></i>",
        label: `Set Stats`,
        callback: saveStateFunction
      },
      no: {
        icon: "<i class='fas fa-times'></i>",
        label: `Cancel`
      }
    }
  }).render(true)

}
