/** 
 * This macro is for Dinosaur Racing in Tomb of Annihilation.
 * It uses custom rules outlined here https://blog.balthazar-rouberol.com/running-the-port-nyanzaru-dinosaur-race
 * 
 * When you run this macro, it will roll on the "Dino Race Loot Box" table
 * It will also reset the table since the "Draw with Replacement" option doesn't seem to do anything.
 */


let table = game.tables.find(t => t.name == "Dino Race Loot Box");
table.draw();
table.reset();
