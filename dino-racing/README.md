# What
These are macros are for the custom dino rules outlined by [Balthazar](https://blog.balthazar-rouberol.com/running-the-port-nyanzaru-dinosaur-race)

# How
1. Copy / Paste the macros from this folder into your Foundry game. 
2. Import the roll table into your Foundry game.
3. Select an actor that will participate in the race
4. Run the `choose-dino-stats` macro to choose predetermined dino stats, or run the `set-dino-stats` macro to specify your own stats.
5. During the race, select an actor and run the `dino-race` macro
6. If the player chooses to lash their dino, run the `lash-dino` macro.
7. If you had previously checked the "Stop prompting" checkbox, you can uncheck it by running the `reset-stop-prompting-flag` macro

I've also created actors for my racers that have the "Jockey WIS" values in Balthazar's blog post, and I set dinosaurs for all of them in the `dino-racer-defaults` macro.
I run the `dino-racer-defaults` macro in step 4 before running `choose-dino-stats` for my players.
I would recommend either creating actors with the same names or editing the `dino-racer-defaults` macro and the `Dinosaur Racing (balthazar)` Journal entry to match your racers.


I also created drawings for the map instead of using the image provided by Balthazar. You can see it [here](screenshots/port-nyanzaru-race-drawings-screenshot.png).

# Screenshots
### Setting Dino Stats on an actor
![choose dino stats prompt screenshot](screenshots/choose-dino-stats-prompt-screenshot.png)  
![set dino stats prompt screenshot](screenshots/set-dino-stats-prompt-screenshot.png)  
![dino stats result screenshot](screenshots/dino-stats-result-screenshot.png)  
### Racing
![dino roll prompt screenshot](screenshots/dino-roll-prompt-screenshot.png)  
![dino race result screenshot](screenshots/dino-race-result-screenshot.png)  
![lash dino result screenshot](screenshots/lash-dino-result-screenshot.png)  
### Loot Box
![loot box result screenshot](screenshots/loot-box-result-screenshot.png)  

