/** 
 * This macro is for Dinosaur Racing in Tomb of Annihilation.
 * It uses custom rules outlined here https://blog.balthazar-rouberol.com/running-the-port-nyanzaru-dinosaur-race
 * 
 * If you had previously checked the "Stop Prompting" flag for a specific racer, this will uncheck it for you.
 */

if (actor) {
  actor.dinoRiderStats.stopPrompting = false;
} else {
  ui.notifications.error("You must have a token selected.");
}
